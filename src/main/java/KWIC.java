import java.util.*;

public class KWIC {
    public static ArrayList<String> Ignored(String input) {
        ArrayList<String> ignore_words = new ArrayList<String>();
        Scanner scan = new Scanner(input);
        while (scan.hasNextLine()) {
            String str = scan.nextLine();
            if (str.equals("::")) {
                break;
            } else
                ignore_words.add(str);
        }
        return ignore_words;
    }

    public static ArrayList<String> Title(String input) {
        ArrayList<String> titles = new ArrayList<String>();
        Scanner scanner = new Scanner(input);
        while (scanner.hasNextLine()) {
            String str = scanner.nextLine();
            if (str.equals("::")) {
                break;
            }
        }
        while (scanner.hasNextLine()) {
            String str_2 = scanner.nextLine();
            titles.add(str_2);
        }
        return titles;
    }
    public static Map<String, ArrayList<String>> Keywords = new HashMap<String, ArrayList<String>>();

    public static Map<String, ArrayList<String>> findKey(List<String> ignored, List<String> title) {
        for (String t : title) {
            t = t.toLowerCase();
            ArrayList<String> word = new ArrayList<>();
            word.addAll(Arrays.asList(t.split("\\s+")));
            int index = 0;
            for (String w : word) {
                ArrayList<String> Neword = new ArrayList<>(word);
                Neword.set(index, w.toUpperCase());
                String new_t = String.join(" ", Neword);
                if (!ignored.contains(w)) {
                    if (Keywords.containsKey(w))
                        Keywords.get(w).add(new_t);
                    else Keywords.put(w, new ArrayList<String>(Arrays.asList(new_t)));
                }
                index++;
            }
        }

        return Keywords;
    }

    public static Map<String, ArrayList<String>> sortKey(Map<String, ArrayList<String>> map) {
        return new TreeMap<String, ArrayList<String>>(map);
    }

    public static String Result(Map<String, ArrayList<String>> map) {
        String s = "";
        for (String key : map.keySet()) {
            for (String title : map.get(key)) {
                s = s + title + "\n";
            }
        }
        return s;
    }
}







